package ru.tsc.goloshchapov.tm.api.repository;

import ru.tsc.goloshchapov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

    List<Task> findAll();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    void clear();

}
