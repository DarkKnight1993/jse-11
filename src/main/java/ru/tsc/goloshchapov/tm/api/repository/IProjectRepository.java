package ru.tsc.goloshchapov.tm.api.repository;

import ru.tsc.goloshchapov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

    List<Project> findAll();

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(Integer index);

    void clear();

}
